﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class VolumeController : MonoBehaviour
{
    public Slider volumeSlider;
    public float volumeValue;
    public float prevVolumeValue;
    public TextMeshProUGUI volumeText;
    public AudioMixer audioMixer;

    public void SetVolume()
    {
        //volumeValue = Mathf.RoundToInt(volumeSlider.value);
        float sliderValue = volumeSlider.value;

        audioMixer.SetFloat("MasterVolume", Mathf.Log10(sliderValue) * 20);

        volumeText.text = (System.Math.Truncate(sliderValue * 100)).ToString();

    }

    public void GetPreviousVolumeValue()
    {
        prevVolumeValue = volumeSlider.value;
    }

    public void SetToPreviousVolumeValue()
    {
        volumeSlider.value = prevVolumeValue;
    }
}
