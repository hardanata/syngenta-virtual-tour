﻿using UnityEngine;

[CreateAssetMenu(fileName = "POI Data", menuName = "Scriptable Objects/POI Data", order = 1)]
public class PoiData : ScriptableObject
{
    public string poiName;
    public string description;
}