﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using DG.Tweening;

public class PanelScaler : MonoBehaviour
{
    private CoroutineHandle spawn, shrink;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPanel()
    {
        spawn = Timing.RunCoroutine(Spawn());
    }
    public void ShrinkPanel()
    {
        shrink = Timing.RunCoroutine(Shrink());
    }
    IEnumerator<float> Spawn()
    {
        if (shrink.IsRunning)
            Timing.KillCoroutines(shrink);
            //yield return Timing.WaitUntilDone(shrink);

        transform.localScale = Vector3.zero;
        gameObject.SetActive(true);
        transform.DOScale(Vector3.one, 0.2f);
        yield return Timing.WaitForSeconds(0.2f);
    }
    IEnumerator<float> Shrink()
    {
        if (spawn.IsRunning)
            Timing.KillCoroutines(spawn);
            //yield return Timing.WaitUntilDone(spawn);

        transform.DOScale(Vector3.zero, 0.2f);
        yield return Timing.WaitForSeconds(0.2f);
        gameObject.SetActive(false);
        transform.localScale = Vector3.one;
    }
}
