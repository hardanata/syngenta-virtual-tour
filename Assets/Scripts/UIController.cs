﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIController : MonoBehaviour
{
    public Camera _camera;
    [Space]
    public Transform panelMainMenu;
    [Space]
    public Transform panelPengaturan;
    public Button buttonPengaturanSimpan, buttonPengaturanBatal;
    [Space]
    public List<Transform> listPanelPanduan;
    [Space]
    public Transform buttonPause;
    [Space]
    public Transform panelPOI;
    public List<Transform> listSubPanelPOI;
    public GameObject buttonNextPanelPOI, buttonBackPanelPOI;
    [Space]
    public TextMeshProUGUI namaPOI;
    public TextMeshProUGUI deskripsiPOI;
    //[Space]
    //public Material defaultMaterial;
    //public Material hoveredMaterial;
    [Space]
    public Sprite defaultSprite;
    public Sprite completedSprite;

    private Transform objectHit;


    [Space]
    [ReadOnly] [SerializeField] private Transform poi;
    [Space]
    [ReadOnly] [SerializeField] private bool hovered = false;
    [ReadOnly] [SerializeField] private bool isPaused = false;
    [ReadOnly] [SerializeField] private bool isOnSettings = false;
    void Awake()
    {
        isPaused = true;
        isOnSettings = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!isPaused)
            CastRayToWorldPoint();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Pause();
            }
            else
            {
                if (!isOnSettings)
                {
                    UnPause();
                }
                else
                {
                    buttonPengaturanBatal.onClick.Invoke();
                }
            }
        }
    }

    public void Pause()
    {
        CheckPanelPanduan();
        panelPOI.GetComponent<PanelScaler>().ShrinkPanel();
        buttonPause.GetComponent<PanelScaler>().ShrinkPanel();
        panelMainMenu.GetComponent<PanelScaler>().SpawnPanel();

        isPaused = true;
        OnExit();
    }
    public void UnPause()
    {
        panelPengaturan.GetComponent<PanelScaler>().ShrinkPanel();
        panelMainMenu.GetComponent<PanelScaler>().ShrinkPanel();
        foreach (Transform child in listPanelPanduan)
        {
            child.GetComponent<PanelScaler>().ShrinkPanel();
        }
        buttonPause.GetComponent<PanelScaler>().SpawnPanel();

        isPaused = false;
    }

    public void SetOnSettings()
    {
        if (!isOnSettings) isOnSettings = true;
        else isOnSettings = false;
    }

    void CheckPanelPanduan()
    {
        //matiin semua panel panduan dulu
        foreach (Transform child in listPanelPanduan)
        {
            child.GetComponent<PanelScaler>().ShrinkPanel();
        }
    }

    void SpawnPanelPOI()
    {

        //populate data poi di panel
        namaPOI.text = poi.GetComponent<Poi>().poiData.poiName;
        deskripsiPOI.text = poi.GetComponent<Poi>().poiData.description;

        //reset keadaan awal panel poi, panel pertama dinyalakan
        listSubPanelPOI[0].localScale = Vector3.one;
        listSubPanelPOI[0].gameObject.SetActive(true);
        
        //matiin panel 2, 3 dst
        for (int i = 1; i < listSubPanelPOI.Count; i++)
        {
            listSubPanelPOI[i].gameObject.SetActive(false);
        }

        //button next/back direset
        buttonBackPanelPOI.SetActive(false);
        buttonNextPanelPOI.SetActive(true);

        //baru panel induk dinyalakan
        panelPOI.GetComponent<PanelScaler>().SpawnPanel();
    }

    private void CastRayToWorldPoint()
    {
        RaycastHit hit;
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 10f, Color.red);

        //kalo pointer lagi diluar ui, terus di klik, panel poi yg kebuka otomatis nutup
        if(!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                panelPOI.GetComponent<PanelScaler>().ShrinkPanel();
            }
        }

        if (Physics.Raycast(ray, out hit, 10f))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                objectHit = hit.transform;

                if (objectHit.tag == "POI")
                {
                    hovered = true;
                    poi = objectHit;
                    Debug.DrawRay(ray.origin, ray.direction * 10f, Color.green);

                    //hover anim
                    poi.GetChild(0).GetChild(0).gameObject.SetActive(true);

                    if (Input.GetMouseButtonDown(0))
                    {
                        poi.GetChild(0).GetChild(1).GetComponent<Image>().sprite = completedSprite;

                        SpawnPanelPOI();
                    }
                }
                else
                {
                    if (hovered == true)
                    {
                        OnExit();
                    }

                }
            }
        }
        else
        {
            if (hovered == true)
            {
                OnExit();
            }
        }
    }

    private void OnExit()
    {
        hovered = false;

        if(poi)
        {
            poi.GetChild(0).GetChild(0).gameObject.SetActive(false);
            poi = null;
        }

    }
}
